﻿// <License>
//     <Copyright> 2019 © NEVAR Technology Solutions </Copyright>
//     <Url> http://lehoangtrinh.com </Url>
//     <Author> Le Hoang Trinh </Author>
//     <Project> NEVAR.Chatbot.Common </Project>
//     <File>
//         <Name> MessengerExtension.cs </Name>
//         <Created> 28/7/2019 - 09:02 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Newtonsoft.Json;

namespace NEVAR.Chatbot.Common
{
   public static class MessengerExtension
   {
      public static string AddMessengerRecipient(this string input, string recipientId)
      {
         input = input.Replace("<RECIPIENT_ID>", recipientId);
         return input;
      }

      public static string AddMessengerMessage(this string input, string text)
      {
         input = input.Replace("<MESSAGE_TEXT>", text);
         return input;
      }

      public static string ToJsonString(this string input)
      {
         input = JsonConvert.SerializeObject(input);
         return input;
      }

      public static string AddAccessToken(this string input, string token)
      {
         input = input.Replace("<ACCESS_TOKEN>", token);
         return input;
      }
   }
}