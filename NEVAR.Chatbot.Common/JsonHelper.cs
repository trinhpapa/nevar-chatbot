﻿// <License>
//     <Copyright> 2019 © NEVAR Technology Solutions </Copyright>
//     <Url> http://lehoangtrinh.com </Url>
//     <Author> Le Hoang Trinh </Author>
//     <Project> NEVAR.Chatbot.Common </Project>
//     <File>
//         <Name> JsonHelper.cs </Name>
//         <Created> 28/7/2019 - 08:56 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Microsoft.AspNetCore.Hosting;

namespace NEVAR.Chatbot.Common
{
   public class JsonHelper
   {
      private readonly IHostingEnvironment _hostingEnvironment;

      public JsonHelper(IHostingEnvironment hostingEnvironment)
      {
         _hostingEnvironment = hostingEnvironment;
      }

      public string ReadFromFile(string filePath)
      {
         if (string.IsNullOrWhiteSpace(filePath)) return null;

         var webRootPath = _hostingEnvironment.WebRootPath;

         var content = System.IO.File.ReadAllText(webRootPath + "/json_templates/" + filePath);

         return content;
      }
   }
}