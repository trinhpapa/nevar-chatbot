﻿// <License>
//     <Copyright> 2019 © NEVAR Technology Solutions </Copyright>
//     <Url> http://lehoangtrinh.com </Url>
//     <Author> Le Hoang Trinh </Author>
//     <Project> NEVAR.Chatbot.Persistence </Project>
//     <File>
//         <Name> ChatbotDbContext.cs </Name>
//         <Created> 27/7/2019 - 22:38 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using MongoDB.Driver;
using NEVAR.Chatbot.Domain.Entities;

namespace NEVAR.Chatbot.Persistence
{
   public class ChatbotDbContext
   {
      private IMongoClient MongoClient { get; }
      private IMongoDatabase MongoDatabase { get; }

      public ChatbotDbContext(string connectionString, string dbName)
      {
         MongoClient = new MongoClient(connectionString);
         MongoDatabase = MongoClient.GetDatabase(dbName);
      }

      public IMongoCollection<BotMessage> BotMessages => MongoDatabase.GetCollection<BotMessage>("BotMessages");
      public IMongoCollection<BotAnswer> BotAnswers => MongoDatabase.GetCollection<BotAnswer>("BotAnswers");
      public IMongoCollection<User> Users => MongoDatabase.GetCollection<User>("Users");
   }
}