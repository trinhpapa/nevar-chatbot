﻿// <License>
//     <Copyright> 2019 © NEVAR Technology Solutions </Copyright>
//     <Url> http://lehoangtrinh.com </Url>
//     <Author> Le Hoang Trinh </Author>
//     <Project> NEVAR.Chatbot.Persistence </Project>
//     <File>
//         <Name> RepositoryBase.cs </Name>
//         <Created> 27/7/2019 - 22:38 </Created>
//     </File>
//     <Summary></Summary>
// <License>

namespace NEVAR.Chatbot.Persistence.Repository.Base
{
   public class RepositoryBase
   {
      protected ChatbotDbContext Context { get; }

      public RepositoryBase(ChatbotDbContext context)
      {
         Context = context;
      }
   }
}