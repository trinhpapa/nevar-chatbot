﻿using MongoDB.Driver;
using NEVAR.Chatbot.Domain.Entities;
using NEVAR.Chatbot.Domain.Enums;
using NEVAR.Chatbot.Persistence.Repository.Base;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Persistence.Repository
{
   public class UserRepository : RepositoryBase
   {
      public UserRepository(ChatbotDbContext context) : base(context)
      {
      }

      public Task<User> CreateAsync(User model)
      {
         Context.Users.InsertOneAsync(model);
         return Task.FromResult(model);
      }

      public Task<User> CreateAsync(string userId, string firstname, string lastname, string profilePicture, string locale, UserGender gender)
      {
         var userModel = new User
         {
            Id = userId,
            Firstname = firstname,
            Lastname = lastname,
            ProfilePicture = profilePicture,
            Locale = locale,
            Gender = gender
         };
         return CreateAsync(userModel);
      }

      public Task<User> SearchByIdAsync(string userId)
      {
         return Context.Users
             .Find(Builders<User>.Filter.Eq("_id", userId))
             .FirstOrDefaultAsync();
      }
   }
}