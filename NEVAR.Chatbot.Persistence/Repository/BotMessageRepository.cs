﻿using MongoDB.Bson;
using MongoDB.Driver;
using NEVAR.Chatbot.Domain.Entities;
using NEVAR.Chatbot.Persistence.Repository.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Persistence.Repository
{
   public class BotMessageRepository : RepositoryBase
   {
      public BotMessageRepository(ChatbotDbContext context) : base(context)
      {
      }

      public Task<BotMessage> CreateAsync(BotMessage model)
      {
         Context.BotMessages.InsertOneAsync(model);
         return Task.FromResult(model);
      }

      public Task<BotMessage> CreateAsync(string key, string answer)
      {
         var botMessage = new BotMessage
         {
            Key = key,
            BotAnswers = new List<BotAnswer>
                {
                    new BotAnswer {Answer = answer}
                }
         };
         return CreateAsync(botMessage);
      }

      public Task UpdateAsync(string messageId, string answer)
      {
         var filter = Builders<BotMessage>.Filter.Eq("_id", ObjectId.Parse(messageId));

         var update = Builders<BotMessage>.Update.Push(nameof(BotMessage.BotAnswers), answer);

         return Context.BotMessages.FindOneAndUpdateAsync(filter, update);
      }

      public Task<List<BotMessage>> SearchAsync(string text)
      {
         text = text.Trim();

         return Context.BotMessages
             .Find(Builders<BotMessage>.Filter.Text(text))
             .ToListAsync();
      }

      public Task<List<BotMessage>> SearchByKeyAsync(string key)
      {
         key = key.Trim();

         return Context.BotMessages
             .Find(Builders<BotMessage>.Filter.Regex("Key", new BsonRegularExpression(key, "i")))
             .ToListAsync();
      }
   }
}