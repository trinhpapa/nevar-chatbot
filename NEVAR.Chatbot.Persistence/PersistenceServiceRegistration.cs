﻿// <License>
//     <Copyright> 2019 © NEVAR Technology Solutions </Copyright>
//     <Url> http://lehoangtrinh.com </Url>
//     <Author> Le Hoang Trinh </Author>
//     <Project> NEVAR.Chatbot.Persistence </Project>
//     <File>
//         <Name> PersistenceServiceRegistration.cs </Name>
//         <Created> 27/7/2019 - 22:38 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Microsoft.Extensions.DependencyInjection;
using NEVAR.Chatbot.Persistence.Repository;

namespace NEVAR.Chatbot.Persistence
{
   public static class PersistenceServiceRegistration
   {
      public static IServiceCollection AddPersistenceService(this IServiceCollection services)
      {
         services.AddTransient<BotMessageRepository>();
         services.AddTransient<UserRepository>();
         return services;
      }
   }
}