using NEVAR.Chatbot.Persistence.Repository;
using Xunit;

namespace NEVAR.Chatbot.Persistence.Test
{
   public class BotMessageRepositoryTest
   {
      [Fact]
      public async void SearchByKey()
      {
         var repo = GetRepository();
         await repo.SearchByKeyAsync("ch�o");
      }

      public static ChatbotDbContext GetDbContext()
      {
         return new ChatbotDbContext("mongodb://103.74.122.34:27017", "nevar_chatbot");
      }

      public static BotMessageRepository GetRepository()
      {
         return new BotMessageRepository(GetDbContext());
      }
   }
}