﻿// <License>
//     <Copyright> 2019 © NEVAR Technology Solutions </Copyright>
//     <Url> http://lehoangtrinh.com </Url>
//     <Author> Le Hoang Trinh </Author>
//     <Project> NEVAR.Chatbot.Api </Project>
//     <File>
//         <Name> FacebookBotController.cs </Name>
//         <Created> 27/7/2019 - 22:38 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NEVAR.Chatbot.Application.Interface;
using NEVAR.Chatbot.Application.ViewModels;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Api.Controllers
{
   [Route("[controller]")]
   [ApiController]
   public class FacebookBotController : ControllerBase
   {
      private IConfiguration Configuration { get; }

      private IMessageHandlerService _messageHandlerService;

      public FacebookBotController(IConfiguration configuration, IMessageHandlerService messageHandlerService)
      {
         Configuration = configuration;
         _messageHandlerService = messageHandlerService;
      }

      public ActionResult Receive()
      {
         var verifyToken = Configuration.GetSection("FacebookSettings").GetSection("VerifyToken").Value;
         var hubMode = HttpContext.Request.Query["hub.mode"].ToString();
         var hubVerifyToken = HttpContext.Request.Query["hub.verify_token"].ToString();
         var hubChallenge = HttpContext.Request.Query["hub.challenge"].ToString();
         if (hubMode != "subscribe" || hubVerifyToken != verifyToken) return NotFound();
         return new JsonResult(int.Parse(hubChallenge));
      }

      [HttpPost]
      public async Task<ActionResult> ReceivePost([FromBody]BotMessenger data)
      {
         if (!ModelState.IsValid) return BadRequest();
         await _messageHandlerService.ReplyMessageAsync(data);
         return Ok();
      }
   }
}