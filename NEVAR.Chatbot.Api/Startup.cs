﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NEVAR.Chatbot.Application;
using NEVAR.Chatbot.Infrastructure;
using NEVAR.Chatbot.Persistence;

namespace NEVAR.Chatbot.Api
{
   public class Startup
   {
      public Startup(IConfiguration configuration)
      {
         Configuration = configuration;
      }

      public IConfiguration Configuration { get; }

      public void ConfigureServices(IServiceCollection services)
      {
         services.AddScoped(provider =>
             new ChatbotDbContext(Configuration["DataBot:ConnectionString"], Configuration["DataBot:DbName"]));

         services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

         services.AddInfrastructureService();

         services.AddPersistenceService();

         services.AddApplicationService();
      }

      public void Configure(IApplicationBuilder app, IHostingEnvironment env)
      {
         if (env.IsDevelopment())
         {
            app.UseDeveloperExceptionPage();
         }
         else
         {
            app.UseHsts();
         }
         app.UseStaticFiles();
         app.UseHttpsRedirection();
         app.UseMvc();
      }
   }
}