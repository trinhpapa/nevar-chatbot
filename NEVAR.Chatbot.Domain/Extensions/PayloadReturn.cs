﻿namespace NEVAR.Chatbot.Domain.Extensions
{
   public class PayloadReturn<T>
   {
      public bool IsSystemMessage { get; set; } = false;

      public string ReplyMessage { get; set; }

      public T PostbackValue { get; set; }
   }
}