﻿namespace NEVAR.Chatbot.Domain.Enums
{
   public enum UserGender
   {
      Male,
      Female,
      Other,
   }
}