﻿namespace NEVAR.Chatbot.Domain.Enums
{
   public enum UserRole
   {
      Admin,
      Staff,
      Client,
   }
}