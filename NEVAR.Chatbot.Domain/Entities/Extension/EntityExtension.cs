﻿using MongoDB.Bson.Serialization.Attributes;

namespace NEVAR.Chatbot.Domain.Entities.Extension
{
   public class EntityExtension<T>
   {
      [BsonId]
      public T Id { get; set; }

      public MetaExtension Meta { get; set; } = new MetaExtension();
   }
}