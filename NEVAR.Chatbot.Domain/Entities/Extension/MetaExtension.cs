﻿using System;

namespace NEVAR.Chatbot.Domain.Entities.Extension
{
   public class MetaExtension
   {
      public DateTime Created { get; set; }
      public DateTime Updated { get; set; }
      public DateTime? Deleted { get; set; }

      public MetaExtension()
      {
         Updated = Created = DateTime.UtcNow;
      }
   }
}