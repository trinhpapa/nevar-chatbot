﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using NEVAR.Chatbot.Domain.Entities.Extension;
using NEVAR.Chatbot.Domain.Enums;
using Newtonsoft.Json;

namespace NEVAR.Chatbot.Domain.Entities
{
   public class User : EntityExtension<string>
   {
      [JsonProperty("first_name")]
      public string Firstname { get; set; }

      [JsonProperty("last_name")]
      public string Lastname { get; set; }

      [JsonProperty("profile_pic")]
      public string ProfilePicture { get; set; }

      [JsonProperty("locale")]
      public string Locale { get; set; }

      [JsonProperty("gender")]
      [BsonRepresentation(BsonType.String)]
      public UserGender Gender { get; set; } = UserGender.Other;

      [BsonRepresentation(BsonType.String)]
      public UserRole Role { get; set; } = UserRole.Client;
   }
}