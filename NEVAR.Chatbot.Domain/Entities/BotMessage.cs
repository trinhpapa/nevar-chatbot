﻿using MongoDB.Bson;
using NEVAR.Chatbot.Domain.Entities.Extension;
using System.Collections.Generic;

namespace NEVAR.Chatbot.Domain.Entities
{
   public class BotMessage : EntityExtension<ObjectId>
   {
      public string Key { get; set; }

      public List<BotAnswer> BotAnswers { get; set; } = new List<BotAnswer>();
   }
}