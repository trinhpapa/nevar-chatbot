﻿using MongoDB.Bson;
using NEVAR.Chatbot.Domain.Entities.Extension;

namespace NEVAR.Chatbot.Domain.Entities
{
   public class BotAnswer : EntityExtension<ObjectId>
   {
      public string Answer { get; set; }
   }
}