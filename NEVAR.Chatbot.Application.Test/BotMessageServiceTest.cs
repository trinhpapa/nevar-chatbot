﻿using NEVAR.Chatbot.Application.Implementation;
using NEVAR.Chatbot.Persistence;
using NEVAR.Chatbot.Persistence.Repository;
using Xunit;

namespace NEVAR.Chatbot.Application.Test
{
   public class BotMessageServiceTest
   {
      [Fact]
      public async void GetAnswerTest()
      {
         var service = GetService();
         await service.GetAnswerAsync("Chào nữa nè");
      }

      public static ChatbotDbContext GetDbContext()
      {
         return new ChatbotDbContext("mongodb://103.74.122.34:27017", "nevar_chatbot");
      }

      public static BotMessageRepository GetRepository()
      {
         return new BotMessageRepository(GetDbContext());
      }

      public static BotMessageService GetService()
      {
         return new BotMessageService(GetRepository());
      }
   }
}