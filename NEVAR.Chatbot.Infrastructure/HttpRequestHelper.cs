﻿// <License>
//     <Copyright> 2019 © NEVAR Technology Solutions </Copyright>
//     <Url> http://lehoangtrinh.com </Url>
//     <Author> Le Hoang Trinh </Author>
//     <Project> NEVAR.Chatbot.Infrastructure </Project>
//     <File>
//         <Name> HttpRequestHelper.cs </Name>
//         <Created> 27/7/2019 - 22:38 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Infrastructure
{
   public class HttpRequestHelper
   {
      public async Task<string> PostRawAsync(string url, string data)
      {
         var request = (HttpWebRequest)WebRequest.Create(url);
         request.ContentType = "application/json";
         request.Method = "POST";
         using (var requestWriter = new StreamWriter(request.GetRequestStream()))
         {
            requestWriter.Write(data);
         }

         var response = await request.GetResponseAsync();
         if (response == null)
            throw new InvalidOperationException("GetResponse returns null");

         using (var sr = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException()))
         {
            return await sr.ReadToEndAsync();
         }
      }

      public async Task<string> GetRawAsync(string url)
      {
         var request = (HttpWebRequest)WebRequest.Create(url);
         request.Method = "GET";

         var response = await request.GetResponseAsync();
         if (response == null)
            throw new InvalidOperationException("GetResponse returns null");

         using (var sr = new StreamReader(response.GetResponseStream() ?? throw new InvalidOperationException()))
         {
            return await sr.ReadToEndAsync();
         }
      }
   }
}