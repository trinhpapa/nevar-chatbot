﻿// <License>
//     <Copyright> 2019 © NEVAR Technology Solutions </Copyright>
//     <Url> http://lehoangtrinh.com </Url>
//     <Author> Le Hoang Trinh </Author>
//     <Project> NEVAR.Chatbot.Infrastructure </Project>
//     <File>
//         <Name> InfrastructureServiceRegistration.cs </Name>
//         <Created> 27/7/2019 - 22:38 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Microsoft.Extensions.DependencyInjection;

namespace NEVAR.Chatbot.Infrastructure
{
   public static class InfrastructureServiceRegistration
   {
      public static IServiceCollection AddInfrastructureService(this IServiceCollection services)
      {
         services.AddTransient<HttpRequestHelper>();
         return services;
      }
   }
}