﻿using NEVAR.Chatbot.Application.ViewModels;
using NEVAR.Chatbot.Domain.Extensions;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Application.Interface
{
   public interface IMessageHandlerService
   {
      //Task<PayloadReturn<BotMessenger>> SystemMessageHandler(BotMessenger message);

      //Task<PayloadReturn<BotMessenger>> AIMessageHandler(BotMessenger model);

      Task ReplyMessageAsync(BotMessenger model);
   }
}