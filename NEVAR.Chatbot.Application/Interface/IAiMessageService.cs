﻿using System.Threading.Tasks;
using NEVAR.Chatbot.Application.ViewModels;
using NEVAR.Chatbot.Domain.Extensions;

namespace NEVAR.Chatbot.Application.Interface
{
   public interface IAiMessageService
   {
      Task<PayloadReturn<BotMessenger>> ReplyMessage(string userId);
   }
}