﻿using NEVAR.Chatbot.Domain.Entities;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Application.Interface
{
   public interface IUserService
   {
      Task<User> GetUserFromFacebook(string userId);

      Task<User> CheckAndGetAsync(string userId);
   }
}