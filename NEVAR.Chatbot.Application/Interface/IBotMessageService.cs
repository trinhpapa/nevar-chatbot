﻿using NEVAR.Chatbot.Application.ViewModels;
using NEVAR.Chatbot.Domain.Extensions;
using System.Threading;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Application.Interface
{
   public interface IBotMessageService
   {
      Task<PayloadReturn<BotMessenger>> GetAnswerAsync(string messageInput, CancellationToken cancellationToken = default);

      Task<PayloadReturn<BotMessenger>> GetStartedPayload(string userId);

      Task<PayloadReturn<BotMessenger>> GetHelperPayload(string userId);
   }
}