﻿namespace NEVAR.Chatbot.Application.ViewModels
{
   public class Attachment
   {
      public string Type { get; set; }

      public Payload Payload { get; set; }

      public string Title { get; set; }

      public string Url { get; set; }
   }
}