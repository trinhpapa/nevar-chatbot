﻿using System.Collections.Generic;

namespace NEVAR.Chatbot.Application.ViewModels
{
   public class Entry
   {
      public string Id { get; set; }
      public string Time { get; set; }
      public List<Messaging> Messaging { get; set; }
   }
}