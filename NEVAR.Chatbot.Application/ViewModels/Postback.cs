﻿namespace NEVAR.Chatbot.Application.ViewModels
{
   public class Postback
   {
      public string Title { get; set; }
      public string Payload { get; set; }
   }
}