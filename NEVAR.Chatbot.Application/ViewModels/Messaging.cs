﻿namespace NEVAR.Chatbot.Application.ViewModels
{
   public class Messaging
   {
      public Sender Sender { get; set; }
      public Recipient Recipient { get; set; }
      public string Timestamp { get; set; }
      public Message Message { get; set; }
      public Postback Postback { get; set; }
   }
}