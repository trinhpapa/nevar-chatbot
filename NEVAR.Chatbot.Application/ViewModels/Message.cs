﻿using System.Collections.Generic;

namespace NEVAR.Chatbot.Application.ViewModels
{
   public class Message
   {
      public string Text { get; set; }

      public string Mid { get; set; }

      public List<Attachment> Attachments { get; set; }
   }
}