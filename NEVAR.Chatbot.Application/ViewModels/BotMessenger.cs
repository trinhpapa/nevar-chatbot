﻿using System.Collections.Generic;

namespace NEVAR.Chatbot.Application.ViewModels
{
   public class BotMessenger
   {
      public string Object { get; set; }
      public List<Entry> Entry { get; set; }
   }
}