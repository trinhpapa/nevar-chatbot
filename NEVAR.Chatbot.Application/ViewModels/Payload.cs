﻿namespace NEVAR.Chatbot.Application.ViewModels
{
   public class Payload
   {
      public string Url { get; set; }
   }
}