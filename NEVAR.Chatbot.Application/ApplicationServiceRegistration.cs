﻿// <License>
//     <Copyright> 2019 © NEVAR Technology Solutions </Copyright>
//     <Url> http://lehoangtrinh.com </Url>
//     <Author> Le Hoang Trinh </Author>
//     <Project> NEVAR.Chatbot.Application </Project>
//     <File>
//         <Name> ApplicationServiceRegistration.cs </Name>
//         <Created> 27/7/2019 - 22:38 </Created>
//     </File>
//     <Summary></Summary>
// <License>

using Microsoft.Extensions.DependencyInjection;
using NEVAR.Chatbot.Application.Implementation;
using NEVAR.Chatbot.Application.Interface;

namespace NEVAR.Chatbot.Application
{
   public static class ApplicationServiceRegistration
   {
      public static IServiceCollection AddApplicationService(this IServiceCollection services)
      {
         services.AddTransient<IBotMessageService, BotMessageService>();
         services.AddTransient<IAiMessageService, AiMessageService>();
         services.AddTransient<IMessageHandlerService, MessageHandlerService>();
         services.AddTransient<IUserService, UserService>();

         return services;
      }
   }
}