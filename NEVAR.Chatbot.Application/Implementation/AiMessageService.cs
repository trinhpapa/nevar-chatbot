﻿using Microsoft.AspNetCore.Hosting;
using NEVAR.Chatbot.Application.Interface;
using NEVAR.Chatbot.Application.ViewModels;
using NEVAR.Chatbot.Common;
using NEVAR.Chatbot.Domain.Extensions;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Application.Implementation
{
   public class AiMessageService : IAiMessageService
   {
      private IHostingEnvironment HostingEnvironment { get; }

      public AiMessageService(IHostingEnvironment hostingEnvironment)
      {
         HostingEnvironment = hostingEnvironment;
      }

      public Task<PayloadReturn<BotMessenger>> ReplyMessage(string userId)
      {
         var returnValue = new PayloadReturn<BotMessenger>
         {
            IsSystemMessage = true,
            ReplyMessage = new JsonHelper(HostingEnvironment)
               .ReadFromFile("messages/text.json")
               .AddMessengerRecipient(userId)
               .AddMessengerMessage("Xin chào")
         };

         return Task.FromResult(returnValue);
      }
   }
}