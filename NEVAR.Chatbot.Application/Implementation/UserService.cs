﻿using Microsoft.Extensions.Configuration;
using NEVAR.Chatbot.Application.Interface;
using NEVAR.Chatbot.Common;
using NEVAR.Chatbot.Domain.Entities;
using NEVAR.Chatbot.Infrastructure;
using NEVAR.Chatbot.Persistence.Repository;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Application.Implementation
{
   public class UserService : IUserService
   {
      private IConfiguration Configuration { get; }
      private readonly HttpRequestHelper _httpRequestHelper;
      private readonly UserRepository _userRepository;

      public UserService(UserRepository userRepository, IConfiguration configuration, HttpRequestHelper httpRequestHelper)
      {
         _userRepository = userRepository;
         Configuration = configuration;
         _httpRequestHelper = httpRequestHelper;
      }

      public async Task<User> GetUserFromFacebook(string userId)
      {
         var token = Configuration.GetSection("FacebookSetting").GetSection("AccessToken").Value;
         var profileUrl = Configuration.GetSection("FacebookSetting").GetSection("ProfileUrl").Value
            .AddMessengerRecipient(userId)
            .AddAccessToken(token);

         return JsonConvert.DeserializeObject<User>(await _httpRequestHelper.GetRawAsync(profileUrl));
      }

      public async Task<User> CheckAndGetAsync(string userId)
      {
         var userFromDb = await _userRepository.SearchByIdAsync(userId);
         if (userFromDb == null)
         {
            return await _userRepository.CreateAsync(await GetUserFromFacebook(userId));
         }

         return userFromDb;
      }
   }
}