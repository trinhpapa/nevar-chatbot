﻿using Microsoft.Extensions.Configuration;
using NEVAR.Chatbot.Application.Interface;
using NEVAR.Chatbot.Application.ViewModels;
using NEVAR.Chatbot.Common;
using NEVAR.Chatbot.Domain.Extensions;
using NEVAR.Chatbot.Infrastructure;
using System;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Application.Implementation
{
   public class MessageHandlerService : IMessageHandlerService
   {
      private IConfiguration Configuration { get; }
      private readonly IBotMessageService _botMessageService;
      private readonly IAiMessageService _aiMessageService;
      private readonly HttpRequestHelper _httpRequestHelper;

      public MessageHandlerService(IBotMessageService botMessageService, IConfiguration configuration, HttpRequestHelper httpRequestHelper, IAiMessageService aiMessageService)
      {
         _botMessageService = botMessageService;
         Configuration = configuration;
         _httpRequestHelper = httpRequestHelper;
         _aiMessageService = aiMessageService;
      }

      protected async Task<PayloadReturn<BotMessenger>> SystemMessageHandler(BotMessenger model)
      {
         foreach (var entry in model.Entry)
         {
            foreach (var message in entry.Messaging)
            {
               if (string.IsNullOrWhiteSpace(message?.Postback?.Payload))
                  return new PayloadReturn<BotMessenger>
                  {
                     IsSystemMessage = false,
                     PostbackValue = model
                  };

               switch (message.Postback?.Payload)
               {
                  case "GET_STARTED":
                     return await _botMessageService.GetStartedPayload(message.Sender.Id);

                  case "GET_HELPER":
                     return await _botMessageService.GetHelperPayload(message.Sender.Id);

                  default:
                     return new PayloadReturn<BotMessenger>
                     {
                        IsSystemMessage = false,
                        PostbackValue = model
                     };
               }
            }
         }

         throw new NullReferenceException();
      }

      protected async Task<PayloadReturn<BotMessenger>> AIMessageHandler(BotMessenger model)
      {
         foreach (var entry in model.Entry)
         {
            foreach (var message in entry.Messaging)
            {
               if (string.IsNullOrWhiteSpace(message?.Message?.Text))
                  return new PayloadReturn<BotMessenger>
                  {
                     PostbackValue = model
                  };

               return await _aiMessageService.ReplyMessage(message.Sender.Id);
            }
         }
         throw new NullReferenceException();
      }

      public async Task ReplyMessageAsync(BotMessenger model)
      {
         var handlerResult = await SystemMessageHandler(model);

         if (!handlerResult.IsSystemMessage)
         {
            handlerResult = await AIMessageHandler(model);
         }

         if (!string.IsNullOrEmpty(handlerResult?.ReplyMessage))
         {
            Console.WriteLine(handlerResult.ReplyMessage);
            var token = Configuration.GetSection("FacebookSetting").GetSection("AccessToken").Value;
            var messagesUrl = Configuration.GetSection("FacebookSetting").GetSection("MessagesUrl").Value.AddAccessToken(token);
            await _httpRequestHelper.PostRawAsync(messagesUrl, handlerResult.ReplyMessage);
         }
      }
   }
}