﻿using Microsoft.AspNetCore.Hosting;
using NEVAR.Chatbot.Application.Interface;
using NEVAR.Chatbot.Application.ViewModels;
using NEVAR.Chatbot.Common;
using NEVAR.Chatbot.Domain.Extensions;
using NEVAR.Chatbot.Persistence.Repository;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace NEVAR.Chatbot.Application.Implementation
{
   public class BotMessageService : IBotMessageService
   {
      private readonly BotMessageRepository _botMessageRepository;
      private readonly IUserService _userService;
      private IHostingEnvironment HostingEnvironment { get; }

      public BotMessageService(BotMessageRepository botMessageRepository, IUserService userService, IHostingEnvironment hostingEnvironment)
      {
         _botMessageRepository = botMessageRepository;
         _userService = userService;
         HostingEnvironment = hostingEnvironment;
      }

      public async Task<PayloadReturn<BotMessenger>> GetAnswerAsync(string messageInput, CancellationToken cancellationToken = default)
      {
         var returnValue = new PayloadReturn<BotMessenger>();
         while (true)
         {
            var messages = await _botMessageRepository.SearchByKeyAsync(messageInput);

            if (messages.Count > 0)
            {
               var random = new Random();
               var rdMess = random.Next(messages.Count);
               var message = messages[rdMess];
               var rdAns = random.Next(message.BotAnswers.Count);
               returnValue.ReplyMessage = message.BotAnswers[rdAns]?.Answer;
               return returnValue;
            }

            await _botMessageRepository.CreateAsync(messageInput, "Xin chào bạn!");
         }
      }

      public async Task<PayloadReturn<BotMessenger>> GetStartedPayload(string userId)
      {
         await _userService.CheckAndGetAsync(userId);

         var returnValue = new PayloadReturn<BotMessenger>
         {
            IsSystemMessage = true,
            ReplyMessage = new JsonHelper(HostingEnvironment)
               .ReadFromFile("messages/started.json")
               .AddMessengerRecipient(userId)
         };

         return returnValue;
      }

      public Task<PayloadReturn<BotMessenger>> GetHelperPayload(string userId)
      {
         var returnValue = new PayloadReturn<BotMessenger>
         {
            IsSystemMessage = true,
            ReplyMessage = new JsonHelper(HostingEnvironment)
               .ReadFromFile("messages/helper.json")
               .AddMessengerRecipient(userId)
         };

         return Task.FromResult(returnValue);
      }
   }
}